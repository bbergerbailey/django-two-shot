from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import (
    create_receipt_form,
    create_category_form,
    create_account_form,
)

# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipts}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = create_receipt_form(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = create_receipt_form()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def expense_list(request):
    expenses = ExpenseCategory.objects.filter(
        owner__username=request.user.username
    )
    context = {"expense_list": expenses}
    return render(request, "receipts/expense_list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner__username=request.user.username)
    context = {"account_list": accounts}
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = create_category_form(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("expense_list")
    else:
        form = create_category_form()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_expense_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = create_account_form(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = create_account_form()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
